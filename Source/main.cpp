/**
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include <chrono>
#include <iostream>

#include "Source/Base/FrameCounter.hpp"
#include "Source/Graphics/DisplayManager.hpp"
#include "Source/Graphics/Implementations/DRMDisplayManager.hpp"

int main() {
    gfx::DRMDisplayManager drmDisplayManager{};
    gfx::DisplayManager *displayManager = &drmDisplayManager;

    if (!displayManager->initialize()) {
        std::cout << "[main] Failed to initialize display manager: " << displayManager->errorMessage() << '\n';
        return EXIT_FAILURE;
    }

    base::FrameCounter frameCounter{[] (std::size_t fps, float deltaTime) {
        std::printf("[Lavender] FPS: %zu (deltaTime=%f)\n", fps, static_cast<double>(deltaTime));
    }};

    while (true) {
        frameCounter.update();

        auto framebuffer = displayManager->framebuffer();
        if (framebuffer.data == nullptr) {
            std::puts("[main] Got invalid data for framebuffer of DisplayManager");
            return EXIT_FAILURE;
        }

        auto col = (rand() % 0x00ffffff) & 0x00ff00ff;
        std::fill(framebuffer.data, &framebuffer.data[framebuffer.width * framebuffer.height], col);

        if (!displayManager->swapBuffers()) {
            std::puts("[main] Failed to let DisplayManager swap buffers");
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}
