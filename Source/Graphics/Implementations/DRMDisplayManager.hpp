/**
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <optional>

#include "Source/Graphics/DisplayManager.hpp"

namespace gfx {

    // This class is made in such a way that it *can* support multiple displays,
    // but there currently is no reason to have that, therefore I won't bother
    // implementing multi-display support.
    class DRMDisplayManager final
            : public DisplayManager {
        struct ConnectorDescriptor {
            std::uint32_t crtcID;

            std::uint32_t width;
            std::uint32_t height;

            void *frontBuffer;
            std::uint32_t frontFBID;

            void *backBuffer;
            std::uint32_t backFBID;

            void *currentDrawBuffer;
        };

    public:
        ~DRMDisplayManager() override;

        [[nodiscard]] inline constexpr std::string_view
        errorMessage() const override {
            return m_errorMessage;
        }

        [[nodiscard]] inline constexpr DisplayFramebuffer
        framebuffer() const override {
            return {m_connector.width, m_connector.height, static_cast<std::uint32_t *>(m_connector.currentDrawBuffer)};
        }

        [[nodiscard]] bool
        initialize() override;

        [[nodiscard]] bool
        swapBuffers() override;

    private:
        struct DRMEvent {
            enum class Type {
                INVALID,

                // Not an actual event type, but the IO error state
                ERROR_IO,

                CRTC_SEQUENCE,
                FLIP_COMPLETE,
                VBLANK,
            };

            Type type{Type::INVALID};
        };

        [[nodiscard]] std::optional<DRMEvent>
        readEvent() const;

        int m_driFile{-1};
        ConnectorDescriptor m_connector{};
        std::string_view m_errorMessage{};

        bool m_hasUnsyncedPageFlip{false};
    };

} // namespace gfx
