/**
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Graphics/Implementations/DRMDisplayManager.hpp"

#include <array>
#include <optional>
#include <vector>

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstdint>

#include <fcntl.h>
#include <sys/poll.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "Source/Base/IteratorDecision.hpp"

#ifdef __linux__
#   include <linux/types.h>
#endif
#ifndef __user
#   define __user
#endif
#include "Include/drm.h"
#include "Include/drm_mode.h"

constexpr const std::size_t kMaximumConnectors = 10;
constexpr const std::size_t kMaximumConnectorEntries = 20;

struct DumbBuffer {
    drm_mode_create_dumb create{};
    drm_mode_map_dumb map{};
    drm_mode_fb_cmd cmd{};
};

[[nodiscard]] std::optional<DumbBuffer>
createDumbBuffer(int dri_fd, const drm_mode_modeinfo &modeInfo) {
    DumbBuffer buffer{};

    // If we create the buffer later, we can get the size of the screen first.
    // This must be a valid mode, so it's probably best to do this after we find
    // a valid crtc with modes.
    buffer.create.width = modeInfo.hdisplay;
    buffer.create.height = modeInfo.vdisplay;
    buffer.create.bpp = 32;
    buffer.create.flags = 0;
    buffer.create.pitch = 0;
    buffer.create.size = 0;
    buffer.create.handle = 0;
    if (ioctl(dri_fd, DRM_IOCTL_MODE_CREATE_DUMB, &buffer.create) != 0) {
        perror("[Gfx/DisplayManager/DRM] DRM_IOCTL_MODE_CREATE_DUMB");
        std::printf("FD=%i\n", dri_fd);
        return std::nullopt;
    }

    buffer.cmd.width = buffer.create.width;
    buffer.cmd.height = buffer.create.height;
    buffer.cmd.bpp = buffer.create.bpp;
    buffer.cmd.pitch = buffer.create.pitch;
    buffer.cmd.depth = 24;
    buffer.cmd.handle = buffer.create.handle;
    if (ioctl(dri_fd,DRM_IOCTL_MODE_ADDFB, &buffer.cmd) != 0) {
        perror("[Gfx/DisplayManager/DRM] DRM_IOCTL_MODE_ADDFB");
        return std::nullopt;
    }

    buffer.map.handle = buffer.create.handle;
    if (ioctl(dri_fd,DRM_IOCTL_MODE_MAP_DUMB, &buffer.map) != 0) {
        perror("[Gfx/DisplayManager/DRM] DRM_IOCTL_MODE_MAP_DUMB");
        return std::nullopt;
    }

    return buffer;
}

namespace gfx {

    struct DRICardInterface {
        inline constexpr explicit
        DRICardInterface(int fd)
                : m_fd(fd) {
        }

        [[nodiscard]] bool
        becomeMaster() const {
            return ioctl(m_fd, DRM_IOCTL_SET_MASTER, 0) == 0;
        }

        template<std::size_t MaxConnectors=10, typename Callback>
        [[nodiscard]] std::string_view
        forEachConnector(Callback callback) {
            drm_mode_card_res res{};
            std::array<std::uint64_t, MaxConnectors> res_fb_buf{}, res_crtc_buf{}, res_conn_buf{}, res_enc_buf{};

            // Get resource counts
            if (ioctl(m_fd, DRM_IOCTL_MODE_GETRESOURCES, &res) != 0) {
                perror("[Gfx/DisplayManager/DRM] DRM_IOCTL_MODE_GETRESOURCES #1");
                return "connector-iterator: failed to get resource counts";
            }
            res.fb_id_ptr = reinterpret_cast<std::uint64_t>(std::data(res_fb_buf));
            res.crtc_id_ptr = reinterpret_cast<std::uint64_t>(std::data(res_crtc_buf));
            res.connector_id_ptr = reinterpret_cast<std::uint64_t>(std::data(res_conn_buf));
            res.encoder_id_ptr = reinterpret_cast<std::uint64_t>(std::data(res_enc_buf));

            // Get resource IDs
            if (ioctl(m_fd, DRM_IOCTL_MODE_GETRESOURCES, &res) != 0) {
                perror("[Gfx/DisplayManager/DRM] DRM_IOCTL_MODE_GETRESOURCES #2");
                return "connector-iterator: failed to get resource IDs";
            }

            std::printf("fbs=%d, crtcs=%d, conns=%d, encs=%d\n", res.count_fbs, res.count_crtcs, res.count_connectors, res.count_encoders);

            for (std::size_t i =  0; i < res.count_connectors; i++) {
                std::array<drm_mode_modeinfo, kMaximumConnectorEntries> conn_mode_buf{};
                std::array<std::uint64_t, kMaximumConnectorEntries> conn_prop_buf{}, conn_propval_buf{}, conn_enc_buf{};

                drm_mode_get_connector conn{};
                conn.connector_id = static_cast<std::uint32_t>(res_conn_buf[i]);

                std::printf("DEBUG: current index is %zu of %u\n", i, res.count_connectors);

                // get connector resource counts
                if (ioctl(m_fd, DRM_IOCTL_MODE_GETCONNECTOR, &conn) != 0) {
                    perror("[Gfx/DisplayManager/DRM] DRM_IOCTL_MODE_GETCONNECTOR resource counts");
                    std::puts("[Gfx/DisplayManager/DRM] Warning: DRM_IOCTL_MODE_GETCONNECTOR (#1) failed");
                    continue;
                }

                conn.modes_ptr = reinterpret_cast<std::uint64_t>(std::data(conn_mode_buf));
                conn.props_ptr = reinterpret_cast<std::uint64_t>(std::data(conn_prop_buf));
                conn.prop_values_ptr = reinterpret_cast<std::uint64_t>(std::data(conn_propval_buf));
                conn.encoders_ptr = reinterpret_cast<std::uint64_t>(std::data(conn_enc_buf));

                // get connector resources
                if (ioctl(m_fd, DRM_IOCTL_MODE_GETCONNECTOR, &conn) != 0) {
                    perror("[Gfx/DisplayManager/DRM] DRM_IOCTL_MODE_GETCONNECTOR specific");
                    std::puts("[Gfx/DisplayManager/DRM] Warning: DRM_IOCTL_MODE_GETCONNECTOR (#2) failed");
                    continue;
                }

                //Check if the connector is OK to use (connected to something)
                if (conn.count_encoders < 1 || conn.count_modes < 1 || !conn.encoder_id || !conn.connection) {
		            printf("DEBUG: encoders=%u modes=%u encoder_id=%u connection=%u\n", conn.count_encoders, conn.count_modes, conn.encoder_id, conn.connection);
                    std::puts("[Gfx/DisplayManager/DRM] Warning: Connector not connected");
                    continue;
                }

                if (callback(i, conn_mode_buf[0], conn) == base::IteratorDecision::BREAK)
                    break;
            }

            return {};
        }

    private:
        const int m_fd{-1};
    };

    DRMDisplayManager::~DRMDisplayManager() {
        if (m_driFile != -1)
            close(m_driFile);
    }

    bool
    DRMDisplayManager::initialize() {
        m_driFile = open("/dev/dri/card0", O_RDWR | O_CLOEXEC);
        if (m_driFile == -1) {
            perror("Failed to open /dev/dri/card0");
            return false;
        }
        std::printf("FD created=%i\n", m_driFile);

        DRICardInterface interface{m_driFile};
        if (!interface.becomeMaster()) {
            perror("[Gfx/DisplayManager/DRM] Failed to become master of DRI Card 0");
            m_errorMessage = "failed to become master of DRI Card 0";
            return false;
        }
        std::printf("FD=%i\n", m_driFile);

        drm_mode_card_res res{};
        std::array<std::uint64_t, kMaximumConnectors> res_fb_buf{}, res_crtc_buf{}, res_conn_buf{}, res_enc_buf{};

        // Get resource counts
        if (ioctl(m_driFile, DRM_IOCTL_MODE_GETRESOURCES, &res) != 0) {
            perror("[Gfx/DisplayManager/DRM] Failed to get resource counts");
            return false;
        }
        assert(res.count_fbs <= std::size(res_fb_buf));
        assert(res.count_crtcs <= std::size(res_crtc_buf));
        assert(res.count_connectors <= std::size(res_conn_buf));
        assert(res.count_encoders <= std::size(res_enc_buf));
        res.fb_id_ptr = reinterpret_cast<std::uint64_t>(std::data(res_fb_buf));
        res.crtc_id_ptr = reinterpret_cast<std::uint64_t>(std::data(res_crtc_buf));
        res.connector_id_ptr = reinterpret_cast<std::uint64_t>(std::data(res_conn_buf));
        res.encoder_id_ptr = reinterpret_cast<std::uint64_t>(std::data(res_enc_buf));
        std::printf("FD=%i\n", m_driFile);

        // Get resource IDs
        if (ioctl(m_driFile, DRM_IOCTL_MODE_GETRESOURCES, &res) != 0) {
            perror("[Gfx/DisplayManager/DRM] Failed to get resource IDs");
            return false;
        }
        std::printf("FD=%i\n", m_driFile);

        std::printf("fbs=%d, crtcs=%d, conns=%d, encs=%d\n", res.count_fbs, res.count_crtcs, res.count_connectors, res.count_encoders);

        std::printf("FD before loop=%i\n", m_driFile);
        bool found{false};
        auto connectorIteratorErrorMessage =
                interface.forEachConnector([&] (std::size_t index, drm_mode_modeinfo modeInfo, drm_mode_get_connector conn) {
            std::printf("forEachConnector callback fd=%i\n", m_driFile);
            auto frontDumbBuffer = createDumbBuffer(m_driFile, modeInfo);
            assert(frontDumbBuffer.has_value());

            m_connector.frontFBID = frontDumbBuffer->cmd.fb_id;
            m_connector.frontBuffer = mmap(nullptr, static_cast<std::size_t>(frontDumbBuffer->create.size),
                    PROT_READ | PROT_WRITE, MAP_SHARED, m_driFile, static_cast<off_t>(frontDumbBuffer->map.offset));
            m_connector.width = frontDumbBuffer->create.width;
            m_connector.height = frontDumbBuffer->create.height;

            auto backDumbBuffer = createDumbBuffer(m_driFile, modeInfo);
            assert(backDumbBuffer.has_value());
            assert(frontDumbBuffer->create.width == backDumbBuffer->create.width);
            assert(frontDumbBuffer->create.height == backDumbBuffer->create.height);
            m_connector.backFBID = backDumbBuffer->cmd.fb_id;
            m_connector.backBuffer = mmap(nullptr, static_cast<std::size_t>(backDumbBuffer->create.size),
                    PROT_READ | PROT_WRITE, MAP_SHARED, m_driFile, static_cast<off_t>(backDumbBuffer->map.offset));

            drm_mode_get_encoder enc{};
            enc.encoder_id = conn.encoder_id;
            if (ioctl(m_driFile, DRM_IOCTL_MODE_GETENCODER, &enc) != 0) {
                perror("[Gfx/DisplayManager/DRM] Failed to get encoder of connector");
		        return base::IteratorDecision::CONTINUE;
	        }

            m_connector.crtcID = enc.crtc_id;

            drm_mode_crtc crtc{};
            crtc.crtc_id = enc.crtc_id;
            if (ioctl(m_driFile, DRM_IOCTL_MODE_GETCRTC, &crtc) != 0) {
                perror("[Gfx/DisplayManager/DRM] Failed to get crtc of connector");
                return base::IteratorDecision::CONTINUE;
            }

            crtc.fb_id = frontDumbBuffer->cmd.fb_id;
            crtc.set_connectors_ptr = reinterpret_cast<std::uint64_t>(&res_conn_buf[index]);
            crtc.count_connectors = 1;
            crtc.mode = modeInfo;
            crtc.mode_valid = 1;
            if (ioctl(m_driFile, DRM_IOCTL_MODE_SETCRTC, &crtc) != 0) {
                perror("[Gfx/DisplayManager/DRM] Failed to set CRTC of connector");
                return base::IteratorDecision::CONTINUE;
	        }

            // The draw buffer should be the back buffer, since the front buffer is
            //  selected as first framebuffer.
            m_connector.currentDrawBuffer = m_connector.backBuffer;

            found = true;
            return base::IteratorDecision::BREAK;
        });

        if (!connectorIteratorErrorMessage.empty()) {
            m_errorMessage = connectorIteratorErrorMessage;
            return false;
        }

        if (!found) {
            m_errorMessage = "failed to find suitable connector";
            return false;
        }

        std::printf("Found suitable connector sized %ux%u\n", m_connector.width, m_connector.height);
        return true;
    }

    [[nodiscard]] inline std::string
    translateDRMEventType(std::uint32_t type) {
        switch (type) {
            case DRM_EVENT_VBLANK: return "DRM_EVENT_VBLANK";
            case DRM_EVENT_FLIP_COMPLETE: return "DRM_EVENT_FLIP_COMPLETE";
            case DRM_EVENT_CRTC_SEQUENCE: return "DRM_EVENT_CRTC_SEQUENCE";
            default: return std::to_string(type);
        }
    }

    std::optional<DRMDisplayManager::DRMEvent>
    DRMDisplayManager::readEvent() const {
        drm_event event{};

        auto bytesToRead = sizeof(event);
        auto bytesRead = read(m_driFile, &event, bytesToRead);
        if (bytesRead != static_cast<int>(bytesToRead)) {
            return DRMEvent{DRMEvent::Type::ERROR_IO};
        }

        std::printf("[Gfx/DisplayManager/DRM] Got event of type: %s length=%u\n",
                    translateDRMEventType(event.type).c_str(), event.length);

        switch (event.type) {
            case DRM_EVENT_VBLANK: {
                drm_event_vblank vBlankEvent{};
                static_assert(offsetof(drm_event_vblank, user_data) == sizeof(event));
                bytesToRead = sizeof(vBlankEvent) - sizeof(event);
                bytesRead = read(m_driFile, &vBlankEvent + sizeof(event), bytesToRead);
                assert(bytesRead == static_cast<int>(bytesToRead));

                std::printf("[Gfx/DisplayManager/DRM]     VBlankEvent.user_data: %zu\n", static_cast<std::size_t>(vBlankEvent.user_data));
                std::printf("[Gfx/DisplayManager/DRM]     VBlankEvent.tv_sec: %u\n", vBlankEvent.tv_sec);
                std::printf("[Gfx/DisplayManager/DRM]     VBlankEvent.tv_usec: %u\n", vBlankEvent.tv_usec);
                std::printf("[Gfx/DisplayManager/DRM]     VBlankEvent.sequence: %u\n", vBlankEvent.sequence);
                std::printf("[Gfx/DisplayManager/DRM]     VBlankEvent.crtc_id: %u\n", vBlankEvent.crtc_id);
                return DRMEvent{DRMEvent::Type::VBLANK};
            }
            case DRM_EVENT_CRTC_SEQUENCE: {
                drm_event_crtc_sequence crtcSequenceEvent{};
                static_assert(offsetof(drm_event_crtc_sequence, user_data) == sizeof(event));
                bytesToRead = sizeof(crtcSequenceEvent) - sizeof(event);
                bytesRead = read(m_driFile, &crtcSequenceEvent + sizeof(event), bytesToRead);
                assert(bytesRead == static_cast<int>(bytesToRead));

                std::printf("[Gfx/DisplayManager/DRM]     CRTCSequenceEvent.user_data: %zu\n", static_cast<std::size_t>(crtcSequenceEvent.user_data));
                std::printf("[Gfx/DisplayManager/DRM]     CRTCSequenceEvent.time_ns: %zu\n", static_cast<std::size_t>(crtcSequenceEvent.time_ns));
                std::printf("[Gfx/DisplayManager/DRM]     CRTCSequenceEvent.sequence: %zu\n", static_cast<std::size_t>(crtcSequenceEvent.sequence));
                return DRMEvent{DRMEvent::Type::CRTC_SEQUENCE};
            }
            case DRM_EVENT_FLIP_COMPLETE:
                return DRMEvent{DRMEvent::Type::FLIP_COMPLETE};
            default:
                return DRMEvent{DRMEvent::Type::INVALID};
        }
    }

    bool
    DRMDisplayManager::swapBuffers() {
        // We have to wait on an event from the driver to let us know the
        // previous frame has been flipped. This means that the display driver
        // is allowed to asynchronously respond to page flips, in which case we
        // have to make sure the previous frame has actually been used.
        if (m_hasUnsyncedPageFlip) {
            pollfd pollFile{
                    .fd = m_driFile,
                    .events = POLLIN,
                    .revents{}
            };

            const auto pollStatus = poll(&pollFile, 1, 1000);
            if (pollStatus < 0) {
                perror("[Gfx/DisplayManager/DRM] Failed to poll DRM file for events");
                return false;
            }

            if (pollStatus == 0) {
                std::puts("[Gfx/DisplayManager/DRM] Error: The display driver did not respond with a page flip event after 1s!");
                return false;
            }

            while (true) {
                const auto event = readEvent();
                if (!event.has_value()) {
                    std::puts("[Gfx/DisplayManager/DRM] Failed to read event :(");
                    return false;
                }

                if (event->type == DRMEvent::Type::FLIP_COMPLETE) {
                    m_hasUnsyncedPageFlip = false;
                    break;
                }
            }
        }

        drm_mode_crtc_page_flip flip{};
        flip.crtc_id = m_connector.crtcID;

        // Request an event for when the page flip has actually happened
        flip.flags = DRM_MODE_PAGE_FLIP_EVENT;

        if (m_connector.currentDrawBuffer == m_connector.frontBuffer) {
            flip.fb_id = m_connector.frontFBID;
            m_connector.currentDrawBuffer = m_connector.backBuffer;
        } else {
            flip.fb_id = m_connector.backFBID;
            m_connector.currentDrawBuffer = m_connector.frontBuffer;
        }

        auto flipStatus = ioctl(m_driFile, DRM_IOCTL_MODE_PAGE_FLIP, &flip);
        if (flipStatus != 0) {
            perror("Failed to flip mode pages");
            return false;
        }

        m_hasUnsyncedPageFlip = true;
        return true;
    }

} // namespace gfx

