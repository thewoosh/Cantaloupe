/**
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

#include <cstdint>

namespace gfx {

    struct DisplayFramebuffer {
        std::uint32_t width;
        std::uint32_t height;
        std::uint32_t *data;
    };

    class DisplayManager {
    public:
        virtual
        ~DisplayManager() = default;

        [[nodiscard]] virtual std::string_view
        errorMessage() const = 0;

        [[nodiscard]] virtual DisplayFramebuffer
        framebuffer() const = 0;

        [[nodiscard]] virtual bool
        initialize() = 0;

        [[nodiscard]] virtual bool
        swapBuffers() = 0;
    };

} // namespace gfx
