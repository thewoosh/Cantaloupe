/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <chrono>

namespace base {

    template <typename Callback>
    class FrameCounter {
    public:
        inline constexpr explicit
        FrameCounter(Callback callback)
                : m_callback(std::move(callback)) {
        }

        inline void
        update() {
            ++m_frameCount;

            const auto thisFrameTime = std::chrono::steady_clock::now();
            const auto deltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(thisFrameTime - m_previousFrameTime).count();
            m_previousFrameTime = thisFrameTime;

            m_secondCounter += deltaTime;
            if (m_secondCounter >= 1.0f) {
                m_secondCounter -= 1;
                m_callback(m_frameCount, deltaTime);
                m_frameCount = 0;
            }
        }

    private:
        Callback m_callback;
        std::size_t m_frameCount{};
        float m_secondCounter{};
        std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> m_previousFrameTime{std::chrono::steady_clock::now()};
    };

} // namespace base
