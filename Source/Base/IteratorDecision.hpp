/**
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace base {

    enum class IteratorDecision {
        CONTINUE,
        BREAK
    };

} // namespace base
