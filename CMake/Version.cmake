# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
# All Rights Reserved.

set(VERSION_MAJOR 1)
set(VERSION_MINOR 0)
set(VERSION_PATCH 0)

add_compile_definitions(CANTALOUPE_VERSION_MAJOR=${VERSION_MAJOR}
						CANTALOUPE_VERSION_MINOR=${VERSION_MINOR}
						CANTALOUPE_VERSION_PATCH=${VERSION_PATCH})
